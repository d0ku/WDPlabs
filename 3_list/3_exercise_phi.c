#include <stdio.h>
#include "3_exercise.h"

int phi (long int n)
{
	int dividers_count=0; // count of final Euler numbers 
	int dividers[100]; //dividers of n 
	int count =0; //how many dividers
	int j=0; //loop control

	if (n==1)
		return 1;
	
	for (int i=2;i<(n/2)+1;i++) //counts dividers of n 
	{
		if (n%i ==0)
		{
			dividers[count]=i;
			count++;
		}
	}

	for (int i=1;i<n;i++) //checks numbers with dividers of n
	{
		while (j<count)
		{
			if (i%dividers[j]==0)
				break;
			else j++;

		}
		if (j==count)
			dividers_count++;
		j=0;
	}
	return dividers_count;

}